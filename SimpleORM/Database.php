<?php

namespace Micro;

class Database
{

	public $pdo = NULL;

	public $type = NULL;

	public $i = '"';

	public $statements = array();

	protected $config = array();

	public static $queries = array();

	public static $last_query = NULL;


	public function __construct(array $config)
	{

		$this->type = current(explode(':', $config['dns'], 2));

		$this->config = $config;

		if($this->type == 'mysql') $this->i = '`';
	}

	public function connect()
	{
		extract($this->config);

		$this->config = NULL;

		$this->pdo = new \PDO($dns, $username, $password, $params);

		$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}

}


